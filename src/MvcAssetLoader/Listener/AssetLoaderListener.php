<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 4/12/14
 * Time: 9:07 PM
 */

namespace MvcAssetLoader\Listener;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;

class AssetLoaderListener extends AbstractListenerAggregate {

    /**
     * Attach the event listener
     * @param EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        $events->attach(MvcEvent::EVENT_RENDER, array($this, 'injectAssets'), 1000);
    }

    /**
     * Injects assets
     * @param MvcEvent $e
     */
    public function injectAssets(MvcEvent $e)
    {
        $package = $e->getApplication()->getServiceManager()->get('PackageAssetLoader');
        $package->loadDefaults();

        $css = $e->getApplication()->getServiceManager()->get('CssAssetLoader');
        $css->attach();

        $java = $e->getApplication()->getServiceManager()->get('JavascriptAssetLoader');
        $java->attach();
    }
} 