<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 4/12/14
 * Time: 9:34 PM
 */

namespace MvcAssetLoader\Loader;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\Uri;
use Zend\Mvc\MvcEvent;
use Zend\Cache\Storage\Adapter\Filesystem;

abstract class AbstractLoader implements ServiceLocatorAwareInterface {

    const ADD_PRE_MVC = 1;
    const ADD_POST_MVC = 2;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var Uri
     */
    protected $urlValidator;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var string
     */
    protected $absolutePath;

    /**
     * @var array
     */
    protected $postMvcFiles = array();

    /**
     * @var array
     */
    protected $preMvcFiles = array();

    /**
     * @var Filesystem
     */
    protected $cacheAdapter;

    /**
     * @var string
     */
    protected $publicPath = '/public';

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        if( $this->config == null )
        {
            $config = $this->getServiceLocator()->get('config');
            $this->config = $config['asset_loader'];
        }

        return $this->config;
    }

    /**
     * Adds a file to load after libraries
     * @param $file
     * @param null $conditions
     * @param int $position
     * @param bool $checkFile
     */
    public function add($file, $conditions = null, $position = self::ADD_POST_MVC, $checkFile = true )
    {
        if ($position == self::ADD_POST_MVC)
        {
            $this->postMvcFiles[$file] = array('load' => true, 'conditional' => $conditions, 'check_file'=>$checkFile);
        }
        else
        {
            $this->preMvcFiles[$file] = array('load' => true, 'conditional' => $conditions, 'check_file'=>$checkFile);
        }
    }

    /**
     * @return MvcEvent
     */
    public function getEvent()
    {
        return $this->getServiceLocator()->get('application')->getMvcEvent();
    }

    /**
     * Gets the name of the module
     * @return string
     */
    public function getModuleName()
    {
        if( $this->getEvent()->getRouteMatch() == null )
        {
            return 'errorModule';
        }

        $parts = $this->getEvent()->getRouteMatch()->getParams();
        $config = $this->getConfig();
        $chunked = explode('\\',$parts['controller']);

        if( isset($config['force_mvc_to_lower']) && $config['force_mvc_to_lower'])
        {
            return strtolower(array_shift($chunked));
        }


        return (array_shift($chunked));
    }

    /**
     * Gets the name of the controller
     * @return string
     */
    public function getControllerName()
    {
        if( $this->getEvent()->getRouteMatch() == null )
        {
            return 'errorController';
        }

        $parts = $this->getEvent()->getRouteMatch()->getParams();
        $config = $this->getConfig();


        $chunked = explode('\\',$parts['controller']);

        if( isset($config['force_mvc_to_lower']) && $config['force_mvc_to_lower'])
        {
            return strtolower(array_pop($chunked));
        }

        return (array_pop($chunked));
    }

    /**
     * Gets the name of the controller
     * @return string
     */
    public function getActionName()
    {
        if( $this->getEvent()->getRouteMatch() == null )
        {
            return 'errorAction';
        }

        $parts = $this->getEvent()->getRouteMatch()->getParams();
        $config = $this->getConfig();

        if( isset($config['force_mvc_to_lower']) && $config['force_mvc_to_lower'])
        {
            return strtolower($parts['action']);
        }

        return ($parts['action']);
    }

    /**
     * @return string
     */
    public function getPublicPath()
    {
        $config = $this->getConfig();

        if( ! isset($config['public_path']))
        {
            return $this->publicPathl;
        }

        return $config['public_path'];
    }

    /**
     * Gets the absolute path
     * @return string
     */
    protected function getAbsolutePath()
    {
        if( $this->absolutePath == null )
        {
            $this->absolutePath = getcwd() . $this->getPublicPath();
        }

        return $this->absolutePath;
    }

    /**
     * Resolves whether a css or javascript file does in fact exist
     * It resolves minified versions to full versions if the minified version does not exist
     * @param string $absolutePath
     * @param string $directory
     * @param string $file
     * @param string $extension
     * @param bool|array $settings
     * @return bool|string  False if file doesn't exist, otherwise the resolved path
     */
    protected function resolveFile( $absolutePath, $directory, $file, $extension, $settings )
    {
        /**
         * Check if we should be resolving the file
         */
        if( is_array($settings) && isset($settings['check_file']) && $settings['check_file'] === false )
        {
            return $file;
        }

        if( $this->urlValidator == null )
        {
            $this->urlValidator = new Uri(array('allowRelative'=>false));
        }

        if( $this->urlValidator->isValid($file) )
        {
            return $file;
        }

        if( file_exists($absolutePath . $directory . $file . $extension))
        {
            return $directory . $file . $extension;
        }
        else
        {
            // well, maybe the file doesn't exist because there is no minified version?
            if( file_exists($absolutePath . $directory . $file . str_replace('.min', '', $extension)))
            {
                // there is no mini version, but there is a regular version!
                return $directory . $file . str_replace('.min', '', $extension);
            }

            // well, maybe no file exists because it is ONLY in a minified version?
            if( file_exists($absolutePath . $directory . $file . '.min' . $extension))
            {
                // there is no regular version, but there is a mini version!
                return $directory . $file . '.min' . $extension;
            }
        }

        return false;
    }

    /**
     * @return Filesystem
     */
    protected function getCacheAdapter()
    {
        if( ! $this->cacheAdapter )
        {
            $this->cacheAdapter = $this->getServiceLocator()->get('AssetLoaderCacheAdapter');
        }

        return $this->cacheAdapter;
    }

    /**
     * Gets the cache name
     * @param $type
     * @return string
     */
    protected function getCacheName($type)
    {
        return strtolower($this->getModuleName() . '_' . $this->getControllerName() . '_' . $this->getActionName() . '_' . $type);
    }
} 